package com.example.OCR;

import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OCRController {
	
	@Autowired
	private OCRService ocrService;
	
	@PostMapping("/ocr")
	public String getTotal(@RequestBody String path) throws IOException {
		String result = ocrService.getOCRTotal(path);
		return result;
	}
	
	
}
