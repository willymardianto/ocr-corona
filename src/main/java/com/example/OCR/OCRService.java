package com.example.OCR;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import javax.imageio.ImageIO;

import org.springframework.stereotype.Service;

import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;
import net.sourceforge.tess4j.util.LoadLibs;
import org.springframework.beans.factory.annotation.Value;

@Service
public class OCRService {
	
        @Value("${classpath:/tessdata}")
        private File tessdataFile;
    
	public String getOCRTotal(String path) throws MalformedURLException, IOException {
		
//            File tessDataFolder = LoadLibs.extractTessResources("tessdata");
//            System.out.println(tessDataFolder.getAbsolutePath());
//                File resourcesDirectory = new File("tessdata");
//                System.out.println(resourcesDirectory.getAbsolutePath());
            
                System.out.println(tessdataFile.toPath().toString());

		String total = "";
		ITesseract instance = new Tesseract();
                instance.setDatapath(tessdataFile.toPath().toString());
//                System.out.println(tessdataFile.toPath().toString());
//                instance.setDatapath(".");
//                instance.setDatapath(tessDataFolder.getAbsolutePath());
//                instance.setDatapath(resourcesDirectory.getAbsolutePath());
		try {
                        URL url = new URL(path);
                        BufferedImage img = ImageIO.read(url);
			String str = instance.doOCR(img).toLowerCase();
			String lines[] = str.split("\\r?\\n");
			
			for(int i=0; i<lines.length; i++) {
				if(lines[i].contains("tot")) {
					total = "";
					String[] splitTotal = lines[i].split("tot");
					char[] words = splitTotal[1].toCharArray();
					for(char word : words) {
						if(Character.isDigit(word)) {
							total = total + word;
						} else {
							continue;
						}
					}
				} else {
					continue;
				}
			}
		} catch (TesseractException e) {
			System.out.println("Exception Details " + e.getMessage());
		}
		
		return total;
	}
	
}
